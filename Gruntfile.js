'use strict';

module.exports = function (grunt) {

    // Load grunt tasks automatically
    require('load-grunt-tasks')(grunt);

    // Time how long tasks take. Can help when optimizing build times
    require('time-grunt')(grunt);

    // Define the configuration for all the tasks
    grunt.initConfig({
        // Project settings
        config: {
            styles:     'assets/styles',
            scripts:    'assets/scripts',
            images:     'assets/images'
        },
        clean: {
            defaults: {
                files: [{
                    dot: true,
                    src: [
                        '<%= config.styles %>/*.css',
                        '<%= config.scripts %>/main.min.js',
                        '.sass-cache'
                    ]
                }]
            },
            templatescache:{
                files: [{
                    dot: true,
                    src: [
                        'cache'
                    ]
                }]
            }
        },
        watch: {
            css: {
                files: ['<%= config.styles %>/**/main.scss'],
                tasks: ['sass', 'autoprefixer'],
                options: {
                    livereload: true
                }
            },
            js: {
                files: ['<%= config.scripts %>/**/*.js'],
                tasks: ['jshint']
            },
            html: {
                files:['templates/**/*.html'],
                tasks: ['clean:templatescache']
            }
        },
        sass: {
            defaults: {
                options: {
                    style: 'compact'
                },
                files: {
                    '<%= config.styles %>/main.css':'<%= config.styles %>/raw/main.scss'
                }
            }
        },
        autoprefixer: {
            options: {
                browsers: ['last 3 version', 'ie 7', 'ie 8', 'ie 9']
            },
            defaults: {
                expand: true,
                flatten: true,
                src: '<%= config.styles %>/main.css',
                dest: '<%= config.styles %>/'
            }

        },
        // Make sure code styles are up to par and there are no obvious mistakes
        jshint: {
            options: {
                jshintrc: '.jshintrc',
                reporter: require('jshint-stylish')
            },
            all: [
                'Gruntfile.js',
                'config.scripts %>/**/*.js'
            ]
        },
        uglify: {
            defaults: {
                files: {
                    '<%= config.scripts %>/main.min.js': ['<%= config.scripts %>/raw/main.js']
                }
            }
        }
    });

    grunt.registerTask('default', [
        'clean',
        'sass',
        'autoprefixer',
        'jshint',
        'uglify',
        'watch'
    ]);

    grunt.registerTask('build', [
        'clean',
        'sass',
        'autoprefixer',
        'jshint',
        'uglify'
    ]);
};